import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class main {

    public static String intoarce(String text){
        String reverse = "";
        for(int i = text.length() - 1; i >= 0; i--)
        {
            reverse = reverse + text.charAt(i);
        }

        return reverse;
    }

    public static void main(String[] args) {
        JFrame f = new JFrame();//creating instance of JFrame

        JButton b = new JButton("click");//creating instance of JButton
        b.setBounds(170, 100, 100, 40);//x axis, y axis, width, height
        JTextField field = new JTextField();

        JTextField field2 = new JTextField();

        JTextField field3 = new JTextField();


        field3.setEditable(false);


        field.setBounds(0,100,150,70);
        field2.setBounds(0,200,150,70);
        field3.setBounds(0,300,150,70);
        f.add(b);//adding button in JFrame
        f.add(field);
        f.add(field2);
        f.add(field3);

        f.setSize(400, 500);//400 width and 500 height
        f.setLayout(null);//using no layout managers
        f.setVisible(true);//making the frame visible

        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                String textFieldValue1 = field.getText();
                String textFieldValue2 = field2.getText();

                int a1 = Integer.parseInt(textFieldValue1);
                int a2 = Integer.parseInt(textFieldValue2);
                int sum = a1+a2;

                field3.setText(String.valueOf(sum));
            }
        });
    }
}
